package tp4;

import java.util.AbstractMap;

public class HashCouple extends AbstractMap.SimpleEntry<String, Integer >{
	private static final long serialVersionUID = 1L;

	public HashCouple(String key, Integer value) {
		super(key, value);
	}
	
	@Override
	public int hashCode() {
		return Math.abs(getKey().hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj))
			return false;
		return true;
	}

}
