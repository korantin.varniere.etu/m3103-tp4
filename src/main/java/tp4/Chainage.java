package tp4;

import java.util.LinkedList;

public class Chainage implements HashTable {
	
	public static final Integer DEFAULT_SIZE = 16;
	private LinkedList<HashCouple>[] table;
	private Integer size;
	
	public Chainage() {
		this.table = new LinkedList[DEFAULT_SIZE];
		for (Integer i = 0; i < this.table.length; ++i) {
			this.table[i] = new LinkedList<HashCouple>();
		}
		this.size = 0;
	}

	@Override
	public int size() {
		return this.size();
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		return this.table[key.hashCode() % this.table.length].indexOf(key) != -1;
	}

	@Override
	public boolean containsValue(Object value) {
		boolean found = false;
		for (int i = 0; i < table.length && !found; i++) {
			LinkedList<HashCouple> linkedList = table[i];
			for (int j = 0; j < linkedList.size() && !found; j++) {
				found = table[i].get(j).equals(value);
			}
		}
		return found;
	}

	@Override
	public Integer get(String key) {
		if (!containsKey(key)) {
			return null;
		}
		return this.table[key.hashCode() % this.table.length]
				.get(this.table[key.hashCode() % this.table.length].indexOf(key)).getValue();
	}

	@Override
	public Integer put(String key, Integer value) {
		HashCouple old = this.table[key.hashCode() % this.table.length].getLast();
		this.table[key.hashCode() % this.table.length].add(new HashCouple(key, value));
		this.size++;
		
		if (old == null) {
			return null;
		}
		return old.getValue();
	}

	@Override
	public Integer remove(Object key) {
		if (!this.containsKey(key)) {
			return null;
		}
		Integer toRet = this.get((String) key);
		
		this.table[key.hashCode() % this.table.length].remove(this.table[key.hashCode() % this.table.length].indexOf(key));
		this.size--;
		
		return toRet;
	}

	@Override
	public void clear() {
		for (int i = 0; i < this.table.length; i++) {
			this.table[i].clear();
		}
		this.size = 0;
	}

}
