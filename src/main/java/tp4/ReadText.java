package tp4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class ReadText {

	private static Map<String, Integer> wordsMap = new HashMap<String, Integer>();
	
	public static void readFile(String path) throws Exception {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(path)))) {
			
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] wordsOfLine = Pattern.compile("\\p{Space}|\\p{Punct}|’").split(line);
				countWords(wordsOfLine);
			}
			
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e1) {
			throw e1;
		}
	}
	
	public static void countWords(String[] words) {
		for (String word : words) {
			if (wordsMap.containsKey(word.toLowerCase())) {
				wordsMap.replace(word.toLowerCase(), wordsMap.get(word.toLowerCase())+1);
			} else {
				wordsMap.put(word.toLowerCase(), 1);
			}
		}
	}
	
	public static void sortWordsMap() {
		wordsMap.remove("");
		wordsMap = sortByValue(wordsMap);
	}
	
	private static Map<String, Integer> sortByValue(Map<String, Integer> unsortMap) {

        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue())*-1;
            }
        });

        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }


        return sortedMap;
    }
	
	public static <K, V> void printMap(Map<K, V> map) {
		int cpt = 0;
		Iterator<Map.Entry<K, V>> ite = map.entrySet().iterator();
		while (ite.hasNext() && cpt < 5) {
			cpt++;
			Map.Entry<K, V> entry = ite.next();
            System.out.println("Key : " + entry.getKey()
                    + " Value : " + entry.getValue());
		}
    }
	
	public static void main(String[] args) {
		try {
			readFile("src/main/resources/tp4/file.txt");
		} catch (Exception e) {e.printStackTrace();}
		
		sortWordsMap();
		
		printMap(wordsMap);
	}
	
}
